<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Route::post('/store','MachineDiaController@store');


Route::get('/delete/{id}','MachineDiaController@delete');
Route::post('/update/{id}','MachineDiaController@updateAjax');



Route::resource('/machineDia', 'MachineDiaController');


Route::auth();

Route::get('/home', 'HomeController@index');
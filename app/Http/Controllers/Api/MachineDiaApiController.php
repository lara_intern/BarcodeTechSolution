<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Http\Response;
use JWTAuth;
use Dingo\Api\Routing\Helpers;
use App\MachineDia;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MachineDiaApiController extends Controller

{
    use Helpers;
    public function index()
    {
        $machineDias = MachineDia::orderBy('created_at', 'DESC')->get();
         return response()->json([$machineDias]);
    }
    public function show($id)
    {
        $machineDia =MachineDia::find($id);
        if(!$machineDia)
            throw new NotFoundHttpException;
        return response()->json([$machineDia]);
    }
    public function store(Request $request)
    {
        $machineDia = new MachineDia;
        $machineDia->machine_dia = $request->get('machine_dia');
        if($machineDia->save())
            return response()->json(['Success']);
        else
        return response()->json(['Oops!! Something wrong to store data.']);
    }
    public function update(Request $request, $id)
    {
        $machineDia = MachineDia::find($id);
        if(!$machineDia)
            throw new NotFoundHttpException;
        $machineDia->update($request->all());
        if($machineDia->save())
            return $this->response->noContent(); //return response()->json(['noContent']);
        else
            return $this->response->error('could_not_update_MachineDia', 500);
    }
    public function destroy($id)
    {
        $machineDia = MachineDia::find($id);
        if(!$machineDia)


            throw new NotFoundHttpException;
        if($machineDia->delete())
            return $this->response->noContent();
        else
            //return $this->response->error('could_not_delete_MachineDia', 500);
        return response()->json(['could_not_delete_MachineDia', 500]);
    }
    private function currentUser() {
        return JWTAuth::parseToken()->authenticate();
    }
}

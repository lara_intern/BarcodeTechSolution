<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\MachineDia;

class MachineDiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $machineDias = MachineDia::orderBy('created_at', 'DESC')->get();
       // return response()->json([$machineDia]);
        return view('settings.machinesSetup.machinedias',compact('machineDias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('machineDia.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {

       //return ($request->machine_dia);
       $machineDia = new MachineDia;
       $machineDia->machine_dia = $request->machine_dia;
       $machineDia->save();
       $all=$this->indexAll();
       return json_encode($all);
        //return view('#.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $machineDia =MachineDia::find($id);
        return json_encode($machineDia);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $machineDia =MachineDia::find($id);
        return view('#',compact('machineDia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $machineDia = MachineDia::find($id);
        if(!$machineDia)
            throw new NotFoundHttpException;
        else
        $machineDia->update($request->all());
        return view('#.index');

    }
    public function indexAll(){
        //return 1;
        $machineDia = MachineDia::orderBy('created_at', 'DESC')->get();
        return $machineDia;
    }
    public function updateAjax(request $request){
        //return $request->id;

        $machineDia = MachineDia::find($request->id);
        if(!$machineDia)
            throw new NotFoundHttpException;
        else
        $machineDia->update($request->all());

        $all=$this->indexAll();
       return json_encode($all);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return ($id);
       // $machineDia = MachineDia::find($id);
       // if(!$machineDia)
       //     throw new NotFoundHttpException;
       // else
       // $machineDia->delete();
       // return view('#.index');
    }
    public function delete($id)
    {
        
        $machineDia = MachineDia::find($id);
        if(!$machineDia)
            throw new NotFoundHttpException;
        else
        $machineDia->delete();
        $all=$this->indexAll();
       return json_encode($all);
    }
}

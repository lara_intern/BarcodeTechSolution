<?php
	
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {

	$api->post('auth/login', 'App\Http\Controllers\Api\AuthController@login');
	$api->post('auth/signup', 'App\Http\Controllers\Api\AuthController@signup');
	$api->post('auth/recovery', 'App\Http\Controllers\Api\AuthController@recovery');
	$api->post('auth/reset', 'App\Http\Controllers\Api\AuthController@reset');

	// example of protected route
	$api->get('protected', ['middleware' => ['api.auth'], function () {		
		return \App\User::all();
    }]);

	// example of free route
	$api->get('free', function() {
		return \App\User::all();
	});

//	Route::resource('/api/machineDia', 'Api\MachineDiaApiController');


	$api->group(['middleware' => 'api.auth'], function ($api) {
		$api->resource('/machineDia', 'App\Http\Controllers\Api\MachineDiaApiController');

	});

});




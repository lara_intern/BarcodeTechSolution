<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineDia extends Model
{
    protected $fillable = ['machine_dia'];
}
